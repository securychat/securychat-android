package com.laljaka.securychat

import kotlinx.coroutines.Job
import org.junit.Assert.*

import org.junit.Test
import kotlin.io.encoding.Base64

class ExtensionFunctionsKtTest {

    @Test
    fun `Given a function that returns a Job should make said function return nothing`() {
        fun mockFunction(): Job {
            return Job()
        }

        val test = mockFunction().ignoreReturn()

        assertSame("Function returned a Job", test, Unit)
    }

    @Test
    fun `Given a list extension function should swap position 5 to 2 in said list`() {
        val mockList = arrayListOf(0, 1, 2, 3, 4, 5, 6)

        mockList.swap(5, 2)

        val checkList = arrayListOf(0, 1, 5, 3, 4, 2, 6)

        assertArrayEquals("Position was not swapped correctly", mockList.toArray(), checkList.toArray())
    }

    @Test
    fun `Given a byte array should insert integer at position in big endian way`() {
        val ba = ByteArray(7)
        val int = -993999994

        ba.insertIntBigEndian(2, int)

        println(ba.contentToString())

        val outInt = (ba[2].correctInt() shl (8 + 8 + 8)) or
                (ba[3].correctInt() shl (8 + 8)) or
                (ba[4].correctInt() shl 8) or
                ba[5].correctInt()

        assertEquals("Integers are not equal", int, outInt)
    }

    @Test
    fun `Given an int should convert it to ByteArray`() {
        val int = 58676
        val ba = int.toByteArray()

        println(ba.contentToString())

        val outInt = (ba[0].correctInt() shl (8 + 8 + 8)) or
                (ba[1].correctInt() shl (8 + 8)) or
                (ba[2].correctInt() shl 8) or
                ba[3].correctInt()

        assertEquals("Integers are not equal", int, outInt)
    }
}