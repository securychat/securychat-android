package com.laljaka.securychat.chatselectorscreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laljaka.securychat.databinding.ChatListRowBinding

class ChatListAdapter(
    private val data: List<ChatListRow>,
) : RecyclerView.Adapter<ChatListRowViewHolder>() {
    private lateinit var listEvent: AdapterRowClicked

    fun setAdapterRowClicked(it: AdapterRowClicked) {
        listEvent = it
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListRowViewHolder {
        //val inflated = LayoutInflater.from(parent.context).inflate(R.layout.chat_list_row, parent, false)
        val binding = ChatListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ChatListRowViewHolder(binding, listEvent)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ChatListRowViewHolder, position: Int) {
        val row = data[position]
        holder.bind(row)
    }
}