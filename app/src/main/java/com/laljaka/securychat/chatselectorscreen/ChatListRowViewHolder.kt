package com.laljaka.securychat.chatselectorscreen

import androidx.recyclerview.widget.RecyclerView
import com.laljaka.securychat.databinding.ChatListRowBinding

class ChatListRowViewHolder(
    private val binding: ChatListRowBinding,
    private val listEvent: AdapterRowClicked
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(row: ChatListRow) {
        binding.tvRowName.text = row.name
        binding.tvRowIP.text = row.ip

        binding.root.setOnClickListener {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listEvent.onClickRow(position)
            }
        }
    }
}