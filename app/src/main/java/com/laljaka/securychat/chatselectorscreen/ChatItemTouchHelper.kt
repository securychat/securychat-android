package com.laljaka.securychat.chatselectorscreen

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class ChatItemTouchHelper: ItemTouchHelper.SimpleCallback(
    ItemTouchHelper.DOWN or ItemTouchHelper.UP,
    ItemTouchHelper.LEFT
) {
    private var onSwipe: ItemTouchHelperSwipe? = null
    private var onDrag: ItemTouchHelperDrag? = null

    fun setItemTouchHelperSwipe(it: ItemTouchHelperSwipe) {
        onSwipe = it
    }

    fun setItemTouchHelperDrag(it: ItemTouchHelperDrag) {
        onDrag = it
    }
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return onDrag?.onDrag(viewHolder.adapterPosition, target.adapterPosition) ?: false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        onSwipe?.onSwipe(viewHolder.adapterPosition)
    }
}