package com.laljaka.securychat.chatselectorscreen

fun interface AdapterRowClicked {
    fun onClickRow(position: Int)
}

fun interface ItemTouchHelperSwipe {
    fun onSwipe(position: Int)
}

fun interface ItemTouchHelperDrag {
    fun onDrag(attacker: Int, victim: Int): Boolean
}