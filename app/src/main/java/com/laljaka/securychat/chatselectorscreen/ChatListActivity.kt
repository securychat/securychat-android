package com.laljaka.securychat.chatselectorscreen

import android.content.Intent
import android.os.Bundle
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.laljaka.securychat.ReceiverService
import com.laljaka.securychat.chatscreen.ChatActivity
import com.laljaka.securychat.databinding.ActivityChatListBinding
import com.laljaka.securychat.loginscreen.MainActivity
import com.laljaka.securychat.swap


class ChatListActivity : AppCompatActivity() {
    //TODO BUG weird lag on going back from chat activity

    private val viewModel: ChatViewModel by viewModels()
    private lateinit var binding: ActivityChatListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityChatListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent2 = Intent(this, ReceiverService::class.java)
        startService(intent2)

        val adapter = ChatListAdapter(viewModel.chatListRows)
        adapter.setAdapterRowClicked {
            val intent = Intent(this@ChatListActivity, ChatActivity::class.java)
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK)
            val bundle = Bundle()
            bundle.putString("name", viewModel.chatListRows[it].name)
            bundle.putString("ip", viewModel.chatListRows[it].ip)
            intent.putExtras(bundle)
            startActivity(intent)
        }

        val helperCallback = ChatItemTouchHelper()
        helperCallback.setItemTouchHelperSwipe {
            viewModel.chatListRows.removeAt(it)
            adapter.notifyItemRemoved(it)
        }
        helperCallback.setItemTouchHelperDrag { attacker, victim ->
            viewModel.chatListRows.swap(attacker, victim)
            adapter.notifyItemMoved(attacker, victim)
            true
        }
        val itemTouchHelper = ItemTouchHelper(helperCallback)
        binding.rvChatList.adapter = adapter
        itemTouchHelper.attachToRecyclerView(binding.rvChatList)
        val layoutManager = LinearLayoutManager(binding.rvChatList.context)
        binding.rvChatList.layoutManager = layoutManager
        binding.rvChatList.addItemDecoration(DividerItemDecoration(
            binding.rvChatList.context,
            layoutManager.orientation
        ))



        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        onBackPressedDispatcher.addCallback(this) {
            logout()
        }

        binding.btAddChat.setOnClickListener {
            viewModel.chatListRows.add(0, ChatListRow("ADDED", "TEST"))
            adapter.notifyItemInserted(0)
            layoutManager.scrollToPosition(0)
        }

        binding.btSettings.setOnClickListener {

        }

    }


    private fun logout() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

}

