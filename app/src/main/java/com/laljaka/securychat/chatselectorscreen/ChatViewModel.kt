package com.laljaka.securychat.chatselectorscreen

import android.util.Log
import androidx.lifecycle.ViewModel

class ChatViewModel: ViewModel() {
    val chatListRows: ArrayList<ChatListRow> = ArrayList()

    init {
        chatListRows.add(ChatListRow("Test1", "Test1"))
        chatListRows.add(ChatListRow("TEST2", "TEST2"))
        Log.d("VIEWMODEL", "Chat vm created")
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("VIEWMODEL", "Chat vm dead")
    }
}