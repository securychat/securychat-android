package com.laljaka.securychat.chatselectorscreen

data class ChatListRow(
    val name: String,
    val ip: String
)
