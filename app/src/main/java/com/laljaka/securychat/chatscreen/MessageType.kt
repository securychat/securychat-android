package com.laljaka.securychat.chatscreen

enum class MessageType {
    SENT,
    RECEIVED
}