package com.laljaka.securychat.chatscreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laljaka.securychat.ObservableList
import com.laljaka.securychat.databinding.MessageReceivedRowBinding
import com.laljaka.securychat.databinding.MessageSentRowBinding

class ChatAdapter(private val data: ObservableList<Message>): RecyclerView.Adapter<ChatViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return data[position].type.ordinal
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        when(viewType) {
            MessageType.SENT.ordinal -> {
                val binding = MessageSentRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return MessageSentViewHolder(binding)
            }
            MessageType.RECEIVED.ordinal -> {
                val binding = MessageReceivedRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return MessageReceivedViewHolder(binding)
            }
        }
        throw IllegalStateException("Impossible view type")
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(data[position])
    }

}