package com.laljaka.securychat.chatscreen

import android.icu.util.Calendar
import androidx.lifecycle.ViewModel
import com.laljaka.securychat.ObservableList

class ChatActivityViewModel: ViewModel() {
    lateinit var chatName: String
    lateinit var chatIp: String
    val messageList = ObservableList<Message>()

    init {
        messageList.add(Message(10000000000, "Test text 1", MessageType.RECEIVED))
        messageList.add(Message(10000000000, "Test text 2", MessageType.RECEIVED))
    }

    fun addMessage(text: String) {
        var calendar = Calendar.getInstance().time.time
        calendar /= 60000
        calendar *= 60000
        val msg = Message(calendar, text, MessageType.SENT)
        messageList.add(msg)
    }
}