package com.laljaka.securychat.chatscreen

data class Message(
    val time: Long,
    val text: String,
    val type: MessageType
)
