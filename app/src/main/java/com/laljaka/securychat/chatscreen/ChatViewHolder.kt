package com.laljaka.securychat.chatscreen

import android.icu.text.SimpleDateFormat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.laljaka.securychat.databinding.MessageReceivedRowBinding
import com.laljaka.securychat.databinding.MessageSentRowBinding
import java.util.Locale


abstract class ChatViewHolder(binding: ViewBinding): RecyclerView.ViewHolder(binding.root) {
    abstract fun bind(message: Message)
}

class MessageSentViewHolder(private val binding: MessageSentRowBinding): ChatViewHolder(binding) {
    override fun bind(message: Message) {
        binding.tvMessageText.text = message.text
        binding.tvTime.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(message.time)//"${(message.time / 3600000) % 24}:${(message.time / 60000) % 60}"
    }
}

class MessageReceivedViewHolder(private val binding: MessageReceivedRowBinding): ChatViewHolder(binding) {
    override fun bind(message: Message) {
        binding.tvMessageText.text = message.text
        binding.tvTime.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(message.time)//"${(message.time / 3600000) % 24}:${(message.time / 60000) % 60}"
    }

}

