package com.laljaka.securychat.chatscreen

import android.os.Bundle
import android.view.KeyEvent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.laljaka.securychat.databinding.ActivityChatBinding

class ChatActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChatBinding
    private val viewModel: ChatActivityViewModel by viewModels()

    //private lateinit var mService: ReceiverService
    //private var mBound: Boolean = false
    /*
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance.
            val binder = service as ReceiverService.ServiceBinder
            mService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

     */

    override fun onStart() {
        super.onStart()
        //Intent(this, ReceiverService::class.java).also { intent ->
        //    bindService(intent, connection, Context.BIND_AUTO_CREATE)
        //}
    }

    override fun onStop() {
        super.onStop()
        //unbindService(connection)
        //mBound = false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //window.setDecorFitsSystemWindows(false)

        val adapter = ChatAdapter(viewModel.messageList)
        val layoutManager = LinearLayoutManager(binding.rvMessages.context).apply {
            stackFromEnd = true
        }

        binding.rvMessages.adapter = adapter
        binding.rvMessages.layoutManager = layoutManager

        viewModel.messageList.observe { index, _ ->
            adapter.notifyItemInserted(index)
            if (layoutManager.findLastVisibleItemPosition() >= adapter.itemCount - 2 || adapter.getItemViewType(adapter.itemCount - 1) == MessageType.SENT.ordinal) {
                layoutManager.scrollToPosition(adapter.itemCount - 1)
            }
        }

        binding.btSend.setOnClickListener {
            val msg = binding.etMessageToSend.text.toString()
            binding.etMessageToSend.text.clear()
            viewModel.addMessage(msg)
        }

        binding.etMessageToSend.addTextChangedListener {
            binding.btSend.isEnabled = !it.isNullOrBlank()
        }

        binding.etMessageToSend.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (binding.btSend.isEnabled) binding.btSend.performClick()
                    return@setOnKeyListener true
                }
            }
            return@setOnKeyListener false
        }

        //binding.rvMessages.itemAnimator.let {
        //    (it as SimpleItemAnimator).supportsChangeAnimations = false
        //}

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.ime())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            //v.setPadding(something.left, something.top, something.right, something.bottom)
            insets
        }
        if (savedInstanceState == null) {
            val bundle = intent.extras!!
            viewModel.chatName = bundle.getString("name", "null")
            viewModel.chatIp = bundle.getString("ip", "null")
        }
        binding.include.tvRowName.text = viewModel.chatName
        binding.include.tvRowIP.text = viewModel.chatIp
    }
}