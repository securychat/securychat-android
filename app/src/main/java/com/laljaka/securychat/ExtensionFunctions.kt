@file:Suppress("UnusedReceiverParameter")

package com.laljaka.securychat

import android.util.Base64
import kotlinx.coroutines.Job
import java.util.Collections

fun Job.ignoreReturn() = Unit

fun <T> ArrayList<T>.swap(i: Int, j: Int) {
    Collections.swap(this, i, j)
}

fun ByteArray.toBase64(): String {
    return Base64.encodeToString(this, Base64.DEFAULT)
}

fun ByteArray.insertIntBigEndian(pos: Int, int: Int) {
    this[pos] = ((int ushr 24) and 0xff).toByte()
    this[pos+1] = ((int ushr 16) and 0xff).toByte()
    this[pos+2] = ((int ushr 8) and 0xff).toByte()
    this[pos+3] = (int and 0xff).toByte()
}

@OptIn(ExperimentalUnsignedTypes::class)
fun UByteArray.insertIntBigEndian(pos: Int, int: Int) {
    this[pos] = ((int ushr 24) and 0xff).toUByte()
    this[pos+1] = ((int ushr 16) and 0xff).toUByte()
    this[pos+2] = ((int ushr 8) and 0xff).toUByte()
    this[pos+3] = (int and 0xff).toUByte()
}

fun Int.toByteArray(): ByteArray {
    val size = 4
    return ByteArray(size) {
        ((this shr ((size - it - 1) * 8)) and 0xff).toByte()
    }
}

/**
 * .toUByte().toInt()
 */
fun Byte.correctInt(): Int {
    return this.toUByte().toInt()
}