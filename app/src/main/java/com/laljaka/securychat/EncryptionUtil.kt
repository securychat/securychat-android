package com.laljaka.securychat

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.security.SecureRandom

@Suppress("unused", "UNUSED_PARAMETER")
object EncryptionUtil {
    init {
        System.loadLibrary("securychat")
        Log.d("D", "Loaded")
    }

    //TODO change library to libsodium wrapper
    external fun init()
    private external fun cGenerateIV(): ByteArray

    fun encrypt(text: String, password: String, salt: String): String {
        /*val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        val iv = generateIV()
        val key = getKeyFromPassword(password, salt)
        cipher.init(Cipher.ENCRYPT_MODE, key, IvParameterSpec(iv))
        val cipherText = cipher.doFinal(text.toByteArray())
        val final = iv + cipherText
        return Base64.getEncoder().encodeToString(final)

         */
        return ""
    }
    fun decrypt(encrypted: String, password: String, salt: String): String {
        /*val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        val base = Base64.getDecoder().decode(encrypted)
        val iv = base.slice(0..15)//mutableListOf<Byte>()
        val encoded = base.drop(16)//mutableListOf<Byte>()
        val key = getKeyFromPassword(password, salt)
        cipher.init(Cipher.DECRYPT_MODE, key, IvParameterSpec(iv.toByteArray()))
        val result = cipher.doFinal(encoded.toByteArray())
        return result.decodeToString()*/
        return ""
    }

    private external fun cGetKeyFromPassword(password: String, salt: ByteArray): ByteArray
    fun getKeyFromPassword(password: String, salt: String): ByteArray {
        val key = cGetKeyFromPassword(password, salt.toByteArray())
        return key
    }

    private external fun cHashPassword(password: String): String
    suspend fun hashPassword(password: String): String = withContext(Dispatchers.Default) {
        val hash = cHashPassword(password)
        hash
    }
    private external fun cVerifyPassword(password: String, hashedPassword: String): Boolean
    suspend fun verifyPassword(password: String, hashedPassword: String): Boolean = withContext(Dispatchers.Default) {
        val res = cVerifyPassword(password, hashedPassword)
        res
    }
    private fun generateIV(): ByteArray {
        val iv = ByteArray(16)
        SecureRandom().nextBytes(iv)
        return iv
    }
}