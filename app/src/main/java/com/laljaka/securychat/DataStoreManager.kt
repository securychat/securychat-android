package com.laljaka.securychat

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map

class DataStoreManager(val it: Application) {
    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
    }

    //private val rememberMeKey = booleanPreferencesKey("remember_me")

    @Suppress("unused")
    suspend fun <T> getKey(key: Preferences.Key<T>): T? {
        return it.dataStore.data.map { value: Preferences ->
            value[key]
        }.firstOrNull()
    }

    fun <T> getFlow(key: Preferences.Key<T>): Flow<T?> {
        return it.dataStore.data.map {
            it[key]
        }
    }

    suspend fun <T> setKey(key: Preferences.Key<T>, value: T) {
        it.dataStore.edit { settings ->
            settings[key] = value
        }
    }

    suspend fun clear() {
        it.dataStore.edit {
            it.clear()
        }
    }
}