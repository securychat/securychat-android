package com.laljaka.securychat.loginscreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.snackbar.Snackbar
import com.laljaka.securychat.R
import com.laljaka.securychat.chatselectorscreen.ChatListActivity
import com.laljaka.securychat.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("STOP", "Created")
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.ime())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        //gestureDetectorCompat = GestureDetectorCompat(this, MyGestureDetector())

        viewModel.decide.observe(this) {
            //if (lifecycle.currentState != Lifecycle.State.RESUMED) return@observe
            if (it) {
                val registerFragment = supportFragmentManager.findFragmentByTag("fragment_register")
                if (registerFragment == null) {
                    Log.d("FRAGMENT", "Could no retrieve register fragment")
                    supportFragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(
                            R.id.fcvLoginOrRegister,
                            RegisterFragment::class.java,
                            null,
                            "fragment_register"
                        )
                    }
                }
            } else {
                val loginFragment = supportFragmentManager.findFragmentByTag("fragment_login")
                if (loginFragment == null) {
                    Log.d("FRAGMENT", "Could no retrieve login fragment")
                    supportFragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(
                            R.id.fcvLoginOrRegister,
                            LoginFragment::class.java,
                            null,
                            "fragment_login"
                        )
                    }
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.newEventBus.collect {
                    when (it) {
                        UIEvent.TO_ACTIVITY -> {
                            val intent = Intent(this@MainActivity, ChatListActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                        UIEvent.ERROR -> {
                            Snackbar.make(findViewById(android.R.id.content), "Wrong", Snackbar.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        Log.d("STOP", "Started")
    }

    override fun onStop() {
        super.onStop()
        Log.d("STOP", "Stopped")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("STOP", "Destroyed")
    }
}