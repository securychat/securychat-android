package com.laljaka.securychat.loginscreen

interface LoginRegisterEvent {
    fun login(password: String)
    fun register(password: String)
}