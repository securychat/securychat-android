package com.laljaka.securychat.loginscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.snackbar.Snackbar
import com.laljaka.securychat.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        //val rootView = inflater.inflate(R.layout.fragment_register, container, false)
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.isDoingRegister.observe(viewLifecycleOwner) {
            if (it) {
                binding.btSavePassword.isEnabled = false
                binding.pbHashing.visibility = View.VISIBLE
            } else {
                binding.btSavePassword.isEnabled = true
                binding.pbHashing.visibility = View.INVISIBLE
            }
        }

        binding.btSavePassword.setOnClickListener {
            val passwordText = binding.etPassword.text.toString()
            if (passwordText.isNotEmpty()) {
                viewModel.register(passwordText)
            } else {
                Snackbar.make(view, "Password can not be empty", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("FRAGMENT", "Register fragment DEAD")
    }
}