package com.laljaka.securychat.loginscreen

import android.util.Log
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.laljaka.securychat.EncryptionUtil
import com.laljaka.securychat.SecurychatApplication.Companion.securychatDataStore
import com.laljaka.securychat.ignoreReturn
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// TODO no hash should be stored, instead a preferences file should be decrypted and if fails a prompt should be shown that says that device might be compromised and ask if the user wants to purge the data
class MainViewModel : ViewModel() {
    private var hash: String? = null
    private val _decide = MutableLiveData<Boolean>()
    val decide: LiveData<Boolean> = _decide

    init {
        Log.d("VIEWMODEL", "Main vm created")
        viewModelScope.launch {
            securychatDataStore.getFlow(stringPreferencesKey("password_hash")).collect {
                _decide.value = it == null
                hash = it
            }
        }
    }

    private val _isDoingLogin = MutableLiveData(false)
    val isDoingLogin: LiveData<Boolean> = _isDoingLogin

    // TODO possibility of remaking everything as state and shared flow

    private val _isDoingRegister = MutableLiveData(false)
    val isDoingRegister: LiveData<Boolean> = _isDoingRegister

    private val _newEventBus = MutableSharedFlow<UIEvent>()
    val newEventBus = _newEventBus.asSharedFlow()


    fun login(password: String) = viewModelScope.launch {
        _isDoingLogin.value = true

        delay(250)

        //_test.value = true
        val res = EncryptionUtil.verifyPassword(password, hash ?: throw IllegalStateException())
        if (res) {
            _newEventBus.emit(UIEvent.TO_ACTIVITY)
        } else {
            _newEventBus.emit(UIEvent.ERROR)
        }
        //delay(2000)
        _isDoingLogin.value = false

        //_test.value = false
    }.ignoreReturn()

    fun register(password: String) = viewModelScope.launch {
        viewModelScope.launch {
            _isDoingRegister.value = true

            delay(250)

            val hash = EncryptionUtil.hashPassword(password)
            withContext(Dispatchers.IO) {
                securychatDataStore.setKey(stringPreferencesKey("password_hash"), hash)
            }
            _isDoingRegister.value = false
        }
    }.ignoreReturn()

    override fun onCleared() {
        super.onCleared()
        Log.d("VIEWMODEL", "Main vm dead")
    }
}