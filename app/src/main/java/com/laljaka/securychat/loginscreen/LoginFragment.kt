package com.laljaka.securychat.loginscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.laljaka.securychat.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.isDoingLogin.observe(viewLifecycleOwner) {
            if (it) {
                binding.btLogin.isEnabled = false
                binding.pbHasing.visibility = View.VISIBLE
            } else {
                binding.btLogin.isEnabled = true
                binding.pbHasing.visibility = View.INVISIBLE
            }
        }


        /*
        lifecycleScope.launch {
            binding.btLoginLoginFragment.isEnabled = true
            binding.pbHashingLoginFragment.visibility = View.INVISIBLE
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.test.collect {
                    Log.d("TIME", "in collector")
                    if (it) {
                        binding.btLoginLoginFragment.isEnabled = false
                        binding.pbHashingLoginFragment.visibility = View.VISIBLE
                        Log.d("TIME", "notify doing work")
                    } else {
                        binding.btLoginLoginFragment.isEnabled = true
                        binding.pbHashingLoginFragment.visibility = View.INVISIBLE
                    }
                }
            }
        }

         */

        /*
        binding.etPassword.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        binding.btLogin.performClick()
                        return@setOnKeyListener true
                    }

                    else -> {}
                }
            }
            return@setOnKeyListener false
        }

         */



        binding.btLogin.setOnClickListener {
            Log.d("TIME", "button pressed")
            val pass = binding.etPassword.text.toString()
            viewModel.login(pass)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("FRAGMENT", "Login fragment DEAD")
    }

}