package com.laljaka.securychat.loginscreen

enum class UIEvent {
    TO_ACTIVITY,
    ERROR
}