package com.laljaka.securychat

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.laljaka.securychat.databinding.FragmentDEBUGBinding


class DEBUGFragment : Fragment() {
    private var _binding: FragmentDEBUGBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDEBUGBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fabDEVDEBUGFragment.setOnClickListener {
            DEBUGDialogFragment().show(childFragmentManager, "DEV")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("FRAGMENT", "DEBUG fragment DEAD")
    }
}