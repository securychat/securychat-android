package com.laljaka.securychat

@Suppress("unused")
class ObservableList<T> {
    fun interface Observer<R> {
        fun emit(index: Int, item: R)
    }

    private val _arr = ArrayList<T>()
    val size get() = _arr.size

    private var observer: Observer<T>? = null

    fun add(index: Int, item: T) {
        _arr.add(index, item)
        observer?.emit(index, item)
    }

    fun add(item: T) {
        val pos = size
        _arr.add(item)
        observer?.emit(pos, item)
    }

    operator fun get(position: Int): T {
        return _arr[position]
    }

    fun observe(it: Observer<T>) {
        observer = it
    }

    fun observe(it: (index: Int, value: T) -> Unit) {
        observer = Observer(it)
    }
}

