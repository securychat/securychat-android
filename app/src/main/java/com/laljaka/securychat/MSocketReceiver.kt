package com.laljaka.securychat

interface MSocketReceiver {
    suspend fun onMessage(message: String)

    suspend fun onConnectionCheck(payload: ByteArray)

    suspend fun onConnectionOK(payload: ByteArray)
}