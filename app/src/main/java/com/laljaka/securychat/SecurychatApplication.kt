package com.laljaka.securychat

import android.app.Application
import android.util.Log

class SecurychatApplication : Application() {
    val keyMap = mapOf<String, String>()

    companion object {
        lateinit var securychatDataStore: DataStoreManager private set
    }

    override fun onCreate() {
        super.onCreate()
        securychatDataStore = DataStoreManager(this)
        EncryptionUtil.init()
        Log.d("Service", "should be started")
    }

    override fun onTerminate() {
        super.onTerminate()
        Log.d("Service", "terminated")
    }


}