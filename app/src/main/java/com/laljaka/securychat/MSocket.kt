package com.laljaka.securychat

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runInterruptible
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.io.OutputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketException
import java.nio.channels.AsynchronousSocketChannel
import java.nio.channels.SocketChannel

class MSocket(host: String, port: Int) {

    private val address = InetSocketAddress(host, port)
    private val socket: Socket = Socket()
    private lateinit var output: OutputStream
    private val header = ByteArray(5)
    private lateinit var input: InputStream
    private lateinit var receiver: MSocketReceiver

    private var isClosing = false
    private val messageChannel: Channel<Protocol.SendOpcodes> = Channel()

    init {
        socket.keepAlive = false
        socket.receiveBufferSize = 212992
        socket.sendBufferSize = 212992
    }

    fun addReceiver(rc: MSocketReceiver) {
        receiver = rc
    }

    fun addReceiver(rc: () -> MSocketReceiver) {
        receiver = rc.invoke()
    }

    suspend fun sendRandomConnCheck() {
        messageChannel.send(Protocol.SendOpcodes.ConnectionCheck())
    }

    suspend fun sendSpecificConnCheck(byteArray: ByteArray) {
        messageChannel.send(Protocol.SendOpcodes.ConnectionCheck(byteArray))
    }


    private fun assertInitialised() {
        if (!socket.isConnected) throw IllegalStateException("Socket needs to be connected first")
        if (!this::receiver.isInitialized) throw IllegalStateException("Receiver must be initialised")
    }

    suspend fun connect() = coroutineScope{
        withContext(Dispatchers.IO) {
            socket.connect(address)
            output = socket.getOutputStream()
            input = socket.getInputStream()
        }

        val job1 = launch { setupReceiver() }
        val job2 = launch { setupSender() }
        joinAllAndOnCancellation(job1, job2) {
            messageChannel.close()
            socket.close()
        }
    }

    private suspend fun setupSender() = withContext(Dispatchers.IO) {
        while (!isClosing) {
            val opcode = try {
                println("before recv")
                val c = messageChannel.receive()
                println("after recv")
                c
            } catch (e : ClosedReceiveChannelException) {
                if (!isClosing) throw e else break
            }

            try {
                output.write(Protocol.buildMessage(opcode))
            } catch (e: SocketException) {
                if (!isClosing) throw e else break
            }

        }
        println("sender ended")

    }

    private suspend fun setupReceiver() = withContext(Dispatchers.IO) {
            // TODO make sure this is cancellable
            while (!isClosing) {
                val n = try {
                    input.read(header)
                } catch (e: SocketException) {
                    if (!isClosing) throw e else break
                }
                if (n == -1) {
                    if (!isClosing) throw SecurityException("WHY -1") else break
                }
                if (n != 5) { throw SecurityException("Read more or less than 5 bytes") }
                val (code, length) = Protocol.parseHeader(header)
                val ba = ByteArray(length)
                if (length > 0) {
                    val m = try {
                        input.read(ba)
                    } catch (e: SocketException) {
                        if (!isClosing) throw e else break
                    }
                    if (m == -1) {
                        if (!isClosing) throw SecurityException("WHY -1") else break
                    }
                    if (m != length) throw SecurityException("Read more or less than required amount bytes")
                }
                when (code) {
                    Protocol.ReceiveOpcodes.RequestPassword -> TODO()
                    Protocol.ReceiveOpcodes.ServerIsPublic -> {}
                    Protocol.ReceiveOpcodes.Close -> {
                        isClosing = true
                        messageChannel.close()
                        socket.close()
                    }
                    Protocol.ReceiveOpcodes.Message -> receiver.onMessage(ba.decodeToString())
                    Protocol.ReceiveOpcodes.ConnectionCheck -> receiver.onConnectionCheck(ba)
                    Protocol.ReceiveOpcodes.ConnectionOK -> receiver.onConnectionOK(ba)
                }
            }
        println("receiver ended")

    }

    suspend fun sendConnCheck(ba: ByteArray?) {
        if (ba != null) messageChannel.send(Protocol.SendOpcodes.ConnectionCheck(ba))
        else messageChannel.send(Protocol.SendOpcodes.ConnectionCheck())
    }

    suspend fun sendConnOK(ba: ByteArray) {
        messageChannel.send(Protocol.SendOpcodes.ConnectionOK(ba))
    }


    suspend fun sendMessage(data: String) {
        messageChannel.send(Protocol.SendOpcodes.Message(data))
        //buildAndSend(Protocol.SendOpcodes.Message(data))
    }

    suspend fun forceClose() {
        withContext(Dispatchers.IO) {
            socket.close()
        }
    }

    suspend fun close() {
        println("closing setting to true")
        isClosing = true
        messageChannel.send(Protocol.SendOpcodes.Close(Protocol.CloseCode.NORMAL))
    }

}