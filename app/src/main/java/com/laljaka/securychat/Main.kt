@file:Suppress("UNUSED_VARIABLE", "UNUSED_PARAMETER", "unused")

package com.laljaka.securychat

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.job
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.Closeable
import java.net.Socket
import java.util.Calendar
import java.util.TimeZone
import kotlin.coroutines.cancellation.CancellationException

@OptIn(ExperimentalUnsignedTypes::class)
fun UByteArray.toByteArray():ByteArray {
    return ByteArray(this.size) {
        this[it].toByte()
    }
}

suspend fun read(s: Socket) {
    withContext(Dispatchers.IO) {
            s.getInputStream().read()

    }
}

suspend fun Job.joinAndOnCancellation(block: () -> Unit) {
    try {
        this.join()
    } catch (e: CancellationException) {
        block()
        throw e
    }
}

suspend fun joinAllAndOnCancellation(vararg job: Job, block: () -> Unit) {
    try {
        job.forEach {
            it.join()
        }
    } catch (e: CancellationException) {
        block()
        throw e
    }
}

fun hhhmain () {
    val job = Job()
    val scope = CoroutineScope(job)
    scope.launch {
        val socket = Socket("localhost", 3939)
        //coroutineScope {


        launch {
            launch(Dispatchers.Default) {
                while(true) {
                    println('b')
                    delay(100)
                }
            }
            launch {
                println("run")
                read(socket)
            }

        }.joinAndOnCancellation { socket.close() }







        //}//.joinAndOnCancellation { socket.close() }

    }
    runBlocking {
        delay(1000)
        job.cancelAndJoin()
    }
}

fun main() {
    val socket = MSocket("localhost", 3939)
    socket.addReceiver(object : MSocketReceiver {
        override suspend fun onMessage(message: String) {
            println(message)
        }

        override suspend fun onConnectionCheck(payload: ByteArray) {
            socket.sendConnOK(payload)
        }

        override suspend fun onConnectionOK(payload: ByteArray) {
            println("connection ok")
        }

    })
    val job = Job()
    val scope = CoroutineScope(job)
    scope.launch {
        socket.connect()
        println("after connect")
        job.complete()
    }


    runBlocking {
        //delay(3000)
        //job.cancel()
        socket.sendMessage("TEST 1fff")
        socket.sendMessage("TEST 2")

        delay(4000)
        socket.sendRandomConnCheck()
        delay(3000)
        socket.close()
        job.join()
    }


}

suspend fun <T : Closeable, R> T.closeOnCancel(block: suspend (T) -> R): R = withContext(Dispatchers.IO) {
    try {
        async { block(this@closeOnCancel) }.await()
    } catch (e: CancellationException) {
        close()
        throw e
    }
}


fun not2main() {
    val nottime = Calendar.getInstance().time.time
    val time = nottime + TimeZone.getDefault().rawOffset
    val offset = 1
    //time += offset
    val seconds = time / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    println("${hours % 24}:${minutes % 60}")

    val a = 8374837483748743
    val b = a / 10000
    val c = b * 10000
    println(a)
    println(b)
    println(c)
}
fun notmain() {
    val test: Long = 10
    val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT")).time.time
    val int: Int = (calendar / 60000).toInt()
    println(int)
    println(0b0111_1111_1111_1111_1111_1111_1111_1111)
    println(int.asHex)
    val ba = ByteArray(Int.SIZE_BYTES)
    println(int.toString(2))
    ba[0] = (int and 0xff).toByte()
    ba[1] = (int shr 8 and 0xff).toByte()
    ba[2] = (int shr 16 and 0xff).toByte()
    ba[3] = (int shr 24).toByte()
    println()
    println(((ba[3].toInt() shl 24) or
             (ba[2].toInt() and 0xff shl 16) or
             (ba[1].toInt() and 0xff shl 8) or
             (ba[0].toInt() and 0xff)))
    println()

    open class Bar
    class Person: Bar()

    val f = { p: Person -> p.toString() }

    fun <T : Bar> withFoo(block: (T) -> String) { }
    fun <T : Bar> otherFoo(block: (T) -> String) { }

    fun main() {
        withFoo(f)
        otherFoo(f)

    }

}





val Int.asBin: String
    get() {
        val n = this
        val maxNum = when {
            n == Int.MIN_VALUE -> 31
            (n xor 0x1) + n == 0x1 -> 0
            (n xor 0xf) + n == 0xf -> 3
            (n xor 0xff) + n == 0xff -> 7
            (n xor 0xfff) + n == 0xfff -> 11
            (n xor 0xffff) + n == 0xffff -> 15
            (n xor 0xfffff) + n == 0xfffff -> 19
            (n xor 0xffffff) + n == 0xffffff -> 23
            (n xor 0xfffffff) + n == 0xfffffff -> 27
            (n xor 0x7fffffff) + n ==0x7fffffff -> 31
            else -> 0
        }
        return (31 downTo 0).fold("") { acc, i ->
            acc + (if ((n ushr i) and 1 == 1) "1" else "0") + if (i % 8 == 0 && i != 0) " " else ""
        }
    }

val Int.asHex: String
    get() = toString(16)