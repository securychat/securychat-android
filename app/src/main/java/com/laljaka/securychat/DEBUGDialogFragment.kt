package com.laljaka.securychat

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import kotlinx.coroutines.runBlocking

class DEBUGDialogFragment : AppCompatDialogFragment() {
    private var chosenItem: Int = 0
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(this.requireContext())
            .setTitle("DEV")
            .setSingleChoiceItems(arrayOf("Clear datastore","Show ram", "Write test file", "Read test file"), chosenItem) { _, checked ->
                chosenItem = checked
            }
            .setNegativeButton("Dismiss") { _,_ ->
                dismiss()
            }
            .setPositiveButton("Do") { _,_ ->
                when (chosenItem) {
                    0 -> runBlocking {
                        SecurychatApplication.securychatDataStore.clear()
                        dismiss()
                    }
                    1 -> {
                        val runtime = Runtime.getRuntime()
                        val usedMem = (runtime.totalMemory() - runtime.freeMemory()) / 1048576L
                        val maxheap = runtime.maxMemory() / 1048576L
                        val free = maxheap - usedMem
                        Toast.makeText(this.requireContext(), free.toString(), Toast.LENGTH_LONG).show()
                    }
                    2 -> {
                        val filename = "myFile"
                        val fileContents = "Hello world!"
                        context?.openFileOutput(filename, Context.MODE_PRIVATE).use {
                            it?.write(fileContents.toByteArray())
                        }
                    }
                    3 -> {
                        context?.openFileInput("myFile")?.bufferedReader()?.useLines { lines ->
                            val something = lines.fold("") { some, text ->
                                some + text
                            }
                            Toast.makeText(this.requireContext(), something, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
            .create()
}