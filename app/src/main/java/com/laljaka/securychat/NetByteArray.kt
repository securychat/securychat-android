package com.laljaka.securychat

class NetByteArray {
    private val internal: ByteArray

    constructor(byteArray: ByteArray) {
        internal = byteArray
    }
    constructor(size: Int) {
        internal = ByteArray(size)
    }

    constructor(size: Int, init: (Int) -> Byte) {
        internal = ByteArray(size, init)
    }
    val size: Int get() = internal.size

    operator fun get(pos: Int): Int {
        return internal[pos].correctInt()
    }

    operator fun set(pos: Int, value: Int) {
        internal[pos] = value.toByte()
    }

    operator fun plus(other: NetByteArray): NetByteArray {
        return NetByteArray(this.internal + other.internal)
    }

    operator fun plus(other: Byte): NetByteArray {
        return NetByteArray(this.internal + other)
    }

    operator fun plus(other: ByteArray): NetByteArray {
        return NetByteArray(this.internal + other)
    }

    fun getRawBytes(): ByteArray {
        return internal
    }

    fun insertIntBigEndian(pos: Int, int: Int) {
        this[pos] = ((int ushr 24) and 0xff)
        this[pos+1] = ((int ushr 16) and 0xff)
        this[pos+2] = ((int ushr 8) and 0xff)
        this[pos+3] = (int and 0xff)
    }

    fun contentToString(): String {
        return internal.joinToString(prefix = "[", postfix = "]") {
            it.toString()
        }
    }

    fun contentToBitString(): String {
        return internal.joinToString(prefix = "[", postfix = "]") {
            it.toUByte().toString(2).padStart(8, '0')
        }
    }

}