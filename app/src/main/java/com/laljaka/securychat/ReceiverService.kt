package com.laljaka.securychat

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import java.net.Socket
import android.net.LocalSocket
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.withStarted
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import java.net.SocketOptions
import java.net.URI

class ReceiverService : Service() {
    //private lateinit var socket: Socket
    //private val lifecycle = CoroutineScope(Dispatchers.IO)
    private val binder = ServiceBinder()
    //private val _eventBus = MutableSharedFlow<Unit>()
    //val eventBus = _eventBus.asSharedFlow()

    inner class ServiceBinder: Binder() {
        fun getService(): ReceiverService = this@ReceiverService
    }



    override fun onBind(intent: Intent): IBinder {
        //super.onBind(intent)
        return binder
    }



    override fun onCreate() {

        super.onCreate()
        Log.d("Service", "started")
        /*lifecycleScope.launch(Dispatchers.IO) {
            val socket = MSocket("10.0.2.2", 3939)

        }

        lifecycle.addObserver(LifecycleEventObserver { _, a ->
            if (a == Lifecycle.Event.ON_DESTROY) {
                //socket.close()
            }
        })

         */
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("Sercancelvice", "start command $intent")

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Log.d("NETWORK", "Service shut down")
        super.onDestroy()
    }
}