package com.laljaka.securychat

import kotlin.random.Random

object Protocol {

    const val REQUEST_PASSWORD = 0b00000
    const val SERVER_IS_PUBLIC = 0b00001
    const val CLOSE = 0b00010
    const val MESSAGE = 0b00011
    const val CONN_CHECK = 0b00100
    const val CONN_OK = 0b00101
    const val CREATE_CHANNEL = 0b00110
    const val JOIN_CHANNEL = 0b00111
    const val ESTABLISH_COMMUNICATION = 0b01000
    const val SEND_SERVER_PASSWORD = 0b01001
    enum class CloseCode(val byte: Byte) {
        NORMAL(100),
        ABRUPT(101)
    }

    private const val VERSION: Int = 0b000

    enum class ReceiveOpcodes(val bits5: Int) {
        RequestPassword(REQUEST_PASSWORD),
        ServerIsPublic(SERVER_IS_PUBLIC),
        Close(CLOSE),
        Message(MESSAGE),
        ConnectionCheck(CONN_CHECK),
        ConnectionOK(CONN_OK)
    }

    sealed class SendOpcodes(val bits5: Int) {
        data class Close(val code: CloseCode): SendOpcodes(CLOSE)
        data class Message(val msg: String): SendOpcodes(MESSAGE)
        data object CreateChannel: SendOpcodes(CREATE_CHANNEL)
        data object JoinChannel: SendOpcodes(JOIN_CHANNEL)
        data object EstablishCommunication: SendOpcodes(ESTABLISH_COMMUNICATION)
        data class ConnectionCheck(val body: ByteArray = Random.nextBytes(4)): SendOpcodes(CONN_CHECK)
        data class ConnectionOK(val body: ByteArray): SendOpcodes(CONN_OK)
        data class SendServerPassword(val body: String): SendOpcodes(SEND_SERVER_PASSWORD)

    }

    fun buildMessage(code: SendOpcodes): ByteArray {
        var ba = NetByteArray(5)
        ba[0] = VERSION shl 5 or code.bits5
        when (code) {
            is SendOpcodes.Close -> ba += code.code.byte
            SendOpcodes.CreateChannel -> {}
            SendOpcodes.EstablishCommunication -> {}
            SendOpcodes.JoinChannel -> {}
            is SendOpcodes.Message -> ba += code.msg.toByteArray()
            is SendOpcodes.ConnectionCheck -> ba += code.body
            is SendOpcodes.ConnectionOK -> ba += code.body
            is SendOpcodes.SendServerPassword -> ba += code.body.toByteArray()
        }
        val size = ba.size - 5
        if (size < 0) throw IllegalStateException("Size can not be less than 0")
        ba.insertIntBigEndian(1, size)
        return ba.getRawBytes()
    }

    fun parseHeader(input: ByteArray): Pair<ReceiveOpcodes, Int> {
        val message = NetByteArray(input)
        val ver = message[0] ushr 5
        val opcode = message[0] and 0b00011111
        assert(ver == VERSION)

        val length: Int = (message[1] shl (8 + 8 + 8)) or
                (message[2] shl (8 + 8)) or
                (message[3] shl 8) or
                message[4]

        val resOpcode: ReceiveOpcodes = when(opcode) {
            ReceiveOpcodes.Close.bits5 -> ReceiveOpcodes.Close
            ReceiveOpcodes.Message.bits5 -> ReceiveOpcodes.Message
            ReceiveOpcodes.ConnectionOK.bits5 -> ReceiveOpcodes.ConnectionOK
            ReceiveOpcodes.ConnectionCheck.bits5 -> ReceiveOpcodes.ConnectionCheck
            ReceiveOpcodes.RequestPassword.bits5 -> ReceiveOpcodes.RequestPassword
            ReceiveOpcodes.ServerIsPublic.bits5 -> ReceiveOpcodes.ServerIsPublic
            else -> throw IllegalStateException("Not supported opcode")
        }

        return Pair(resOpcode, length)
    }
}