#include <jni.h>
#include <string.h>
#include <stdlib.h>

extern "C" {
#include <sodium.h>
}

// Write C++ code here.
//
// Do not forget to dynamically load the C++ library into your application.
//
// For instance,
//
// In MainActivity.java:
//    static {
//       System.loadLibrary("securychat");
//    }
//
// Or, in MainActivity.kt:
//    companion object {
//      init {
//         System.loadLibrary("securychat")
//      }
//    }
extern "C"
JNIEXPORT void JNICALL
Java_com_laljaka_securychat_EncryptionUtil_init(JNIEnv *env, jobject thiz) {
    if (sodium_init() < 0) {
        jclass Exception = env->FindClass("java/lang/IllegalStateException");
        env->ThrowNew(Exception, "Can not initialise libsodium");
    }
}
extern "C"
JNIEXPORT jbyteArray JNICALL
Java_com_laljaka_securychat_EncryptionUtil_cGenerateIV(JNIEnv *env, jobject thiz) {
    if (sodium_init() < 0) {
        jclass Exception = env->FindClass("java/lang/IllegalStateException");
        env->ThrowNew(Exception, "Can not initialise libsodium");
        return nullptr;
    }
    unsigned char salt[crypto_pwhash_SALTBYTES];
    randombytes_buf(salt, sizeof salt);
    jbyteArray jbArray = env->NewByteArray(sizeof salt);
    env->SetByteArrayRegion(jbArray, 0, sizeof salt, reinterpret_cast<const jbyte *>(salt));
    return jbArray;
}
extern "C"
JNIEXPORT jbyteArray JNICALL
Java_com_laljaka_securychat_EncryptionUtil_cGetKeyFromPassword(JNIEnv *env, jobject thiz,
                                                               jstring password, jbyteArray salt) {
    if (sodium_init() < 0) {
        jclass Exception = env->FindClass("java/lang/IllegalStateException");
        env->ThrowNew(Exception, "Can not initialise libsodium");
        return nullptr;
    }
    //TODO salt should be generated with no salt password derivation algo
    if (env->GetArrayLength(salt) != crypto_pwhash_SALTBYTES) {
        jclass Exception = env->FindClass("java/lang/IllegalArgumentException");
        env->ThrowNew(Exception, "Salt size invalid");
        return nullptr;
    }

    unsigned char key[crypto_aead_aegis256_KEYBYTES];
    const char *nativePassword = env->GetStringUTFChars(password, 0);

    jsize len = env->GetArrayLength(salt);
    jbyte *body = env->GetByteArrayElements(salt, 0);
    unsigned char *nativeSalt = new unsigned char[len];
    for (jint i = 0; i < len; i++) {
        nativeSalt[i] = (unsigned char) body[i];
    }

    if (crypto_pwhash
                (key, sizeof key, nativePassword, strlen(nativePassword), nativeSalt,
                 crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE,
                 crypto_pwhash_ALG_DEFAULT) != 0) {
        env->ReleaseByteArrayElements(salt, body, 0);
        env->ReleaseStringUTFChars(password, nativePassword);
        jclass Exception = env->FindClass("java/lang/OutOfMemoryError");
        env->ThrowNew(Exception, "What the actual fuck, out of mem???");
        return nullptr;
    }

    env->ReleaseByteArrayElements(salt, body, 0);
    env->ReleaseStringUTFChars(password, nativePassword);

    jbyteArray jKey = env->NewByteArray(sizeof key);
    env->SetByteArrayRegion(jKey, 0, sizeof key, reinterpret_cast<const jbyte *>(key));
    return jKey;
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_laljaka_securychat_EncryptionUtil_cHashPassword(JNIEnv *env, jobject thiz,
                                                         jstring password) {
    if (sodium_init() < 0) {
        jclass Exception = env->FindClass("java/lang/IllegalStateException");
        env->ThrowNew(Exception, "Can not initialise libsodium");
        return nullptr;
    }

    char hashed_password[crypto_pwhash_STRBYTES];

    const char *nativePassword = env->GetStringUTFChars(password, 0);
    //sodium_mlock(&nativePassword, sizeof nativePassword)

    if (crypto_pwhash_str
                (hashed_password, nativePassword, strlen(nativePassword),
                 3U, 398450861U/*536870912U1073741824U*/) != 0) {
        env->ReleaseStringUTFChars(password, nativePassword);
        jclass Exception = env->FindClass("java/lang/OutOfMemoryError");
        env->ThrowNew(Exception, "What the actual fuck, out of mem???");
        return nullptr;
    }


    //sodium_munlock(&nativePassword, sizeof nativePassword)

    env->ReleaseStringUTFChars(password, nativePassword);

    return env->NewStringUTF(hashed_password);
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_com_laljaka_securychat_EncryptionUtil_cVerifyPassword(JNIEnv *env, jobject thiz,
                                                           jstring password,
                                                           jstring hashedPassword) {
    if (sodium_init() < 0) {
        jclass Exception = env->FindClass("java/lang/IllegalStateException");
        env->ThrowNew(Exception, "Can not initialise libsodium");
        return false;
    }

    const char *nativePassword = env->GetStringUTFChars(password, 0);
    const char *nativeHashedPassword = env->GetStringUTFChars(hashedPassword, 0);


    if (crypto_pwhash_str_verify
                (nativeHashedPassword, nativePassword, strlen(nativePassword)) != 0) {
        env->ReleaseStringUTFChars(password, nativePassword);
        env->ReleaseStringUTFChars(hashedPassword, nativeHashedPassword);
        return false;
    } else {
        env->ReleaseStringUTFChars(password, nativePassword);
        env->ReleaseStringUTFChars(hashedPassword, nativeHashedPassword);
        return true;
    }
}