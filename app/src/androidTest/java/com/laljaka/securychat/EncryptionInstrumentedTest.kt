package com.laljaka.securychat

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EncryptionInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.laljaka.securychat", appContext.packageName)
    }

    @Test
    fun `Given password when hashed should be verifiable with the same password but unverifiable with another`() {
        val password = "somePassword"

        val storedHash = runBlocking { EncryptionUtil.hashPassword(password) }
        val result = runBlocking { EncryptionUtil.verifyPassword(password, storedHash) }

        val wrongPassword = "someOtherPassword"

        val wrongResult = runBlocking { EncryptionUtil.verifyPassword(wrongPassword, storedHash) }

        assertTrue("Hash could not verify the password", result)
        assertFalse("Hash verified the wrong password", wrongResult)
    }
}