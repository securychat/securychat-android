## IMPORTANT

Build each library in their respective .{NAME} folder into {NAME} folder, where {NAME} is the name of the library.
Each library should have its own android build workflow described on its respective page.

After all the manipulations the structure of the {NAME} folder should be like this:
```
{NAME}
├── incude
│   └── ...                 # header files for the library
├── lib   
│   ├── arm64-v8a
│   │   └── {NAME}.a
│   ├── armeabi-v7a
│   │   └── {NAME}.a
│   ├── x86
│   │   └── {NAME}.a
│   ├── x86_64
│   │   └── {NAME}.a
│   └──────
└──────────
```
